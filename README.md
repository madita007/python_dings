## Python automation task
### Control Your Computer, Simplify Your Life 

In this project we can connect to our database and then check for explicit customers and also add and delete new ones.
Besides that it is also possible to save all the data from our table in a separate .txt file.
All the function is also accessible from the command line, once you're connected.

### Setup:
Start your database development environment, for example MySQLworkbench. Then, do the following steps:
1. Execute the sql_dumb_database_create.sql file
2. Execute the projectdatabaseload.sql file
3. Execute the createRoles.sql file
4. Execute the createUsers.sql file

After that, you can go into your IDE and start the commandLine.py file